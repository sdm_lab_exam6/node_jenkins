CREATE TABLE Movie (
  id INTEGER PRIMARY KEY auto_increment,
  title VARCHAR(30),
  release_date Date,
 movie_time time,
  director_name VARCHAR(30)
);
