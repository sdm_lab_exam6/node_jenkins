const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");


router.get("/:title", (request, response) => {
  const { title } = request.params;

  const connection = utils.openConnection();

  const statement = `
        select * from Movie where
        title = '${title}'
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not found !");
    }
  });
});

router.post("/add", (request, response) => {
  const { title, release_date, movie_time,director_name } = request.body;

  const connection = utils.openConnection();
  console.log(connection);
  const statement = `
        insert into Movie
          (title ,release_date,movie_time,director_name)
        values
          ( '${title}',${release_date},${movie_time},${director_name})
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:id", (request, response) => {
  const { id } = request.params;
  const { release_date } = request.body;
  const{movie_time}=request.body;

  const statement = `
    update Movie
    set
      release_date=${release_date}
      movie_time=${movie_time}
    where
      title = '${title}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);

    response.send(utils.createResult(error, result));
  });
});
router.delete("/remove/:title", (request, response) => {
  const { title } = request.params;
  const statement = `
    delete from Movie
    where
      title = '${title}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);
    response.send(utils.createResult(error, result));
  });
});
module.exports = router;
